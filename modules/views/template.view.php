<!DOCTYPE html>
<?php
$page = (isset($_GET['page']) && $_GET['page']) ? $_GET['page'] : 'main-home';
$selectedstore = isset($_SESSION["selectedstore"]) ? $_SESSION["selectedstore"] : "";
$temp = explode('-',$page);
$dir = $temp[0]=="main" ? "" : $temp[0];
$page = $temp[1];
?>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
  <meta name="author" content="Creative Tim">
  <title>Kopi Kelud Pos System</title>
  <!-- Favicon -->
  <!-- <link rel="icon" href="../resource/img/brand/favicon.png" type="image/png"> -->
  <!-- Fonts -->
  <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700"> -->
  <!-- Icons -->
  <link rel="stylesheet" href="../resource/vendor/nucleo/css/nucleo.css" type="text/css">
  <link rel="stylesheet" href="../resource/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
  <!-- Page plugins -->
  <link rel="stylesheet" href="../resource/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="../resource/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">
  <link rel="stylesheet" href="../resource/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css">
  <link rel="stylesheet" href="../resource/vendor/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="../resource/vendor/fullcalendar/dist/fullcalendar.min.css">
  <link rel="stylesheet" href="../resource/vendor/sweetalert2/dist/sweetalert2.min.css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="../resource/css/argon.css?v=1.1.0" type="text/css">
  <link rel="stylesheet" href="../resource/css/style.css" type="text/css">
  <!-- Core -->
  <script src="../resource/vendor/jquery/dist/jquery.min.js"></script>
  <script src="../resource/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="../resource/vendor/js-cookie/js.cookie.js"></script>
  <script src="../resource/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="../resource/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>

  <script src="../resource/vendor/select2/dist/js/select2.min.js"></script>
  <script src="../resource/vendor/moment/min/moment.min.js"></script>
  <script src="../resource/vendor/fullcalendar/dist/fullcalendar.min.js"></script>
  <script src="../resource/vendor/sweetalert2/dist/sweetalert2.min.js"></script>


</head>

<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header d-flex align-items-center">
        <a class="navbar-brand" href="">
          <img src="../resource/img/brand/black.png" class="navbar-brand-img" alt="...">
        </a>
        <div class="ml-auto">
          <!-- Sidenav toggler -->
          <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <h6 class="navbar-heading p-0 text-muted">Home Dashboard</h6>
          <!-- Navigation -->
          <ul class="navbar-nav mb-md-3">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo PATH; ?>?page=main-home">
                <i class="ni ni-app"></i>
                <span class="nav-link-text">Home Dashboard</span>
              </a>
            </li>
          </ul>
          <?php if ($selectedstore!==""): ?>
            <!-- Heading -->
            <h6 class="navbar-heading p-0 text-muted">Pos System</h6>
            <!-- Navigation -->
            <ul class="navbar-nav mb-md-3">
              <li class="nav-item">
                <a class="nav-link" href="<?php echo PATH; ?>?page=pos-kasir">
                  <i class="ni ni-cart"></i>
                  <span class="nav-link-text">Kasir POS</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo PATH; ?>?page=pos-transaksi">
                  <i class="ni ni-bag-17"></i>
                  <span class="nav-link-text">Transaksi</span>
                </a>
              </li>
            </ul>
            <h6 class="navbar-heading p-0 text-muted">Kopi Kelud</h6>
          <?php endif; ?>
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="#navbar-dashboards" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-dashboards">
                <i class="ni ni-shop text-primary"></i>
                <span class="nav-link-text">Store & Home</span>
              </a>
              <div class="collapse <?php if ($dir=="store") echo "show"; ?>" id="navbar-dashboards">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=store-store" class="nav-link">My Store</a>
                  </li>
                  <?php if ($selectedstore!==""): ?>
                    <li class="nav-item">
                      <a href="<?php echo PATH; ?>?page=store-dashboard" class="nav-link">Store Dashboard</a>
                    </li>
                    <li class="nav-item">
                      <a href="<?php echo PATH; ?>?page=store-datakasir" class="nav-link">Transaksi Kasir</a>
                    </li>
                    <li class="nav-item">
                      <a href="<?php echo PATH; ?>?page=store-kelola" class="nav-link">Kelola Store</a>
                    </li>
                    <li class="nav-item">
                      <a href="<?php echo PATH; ?>?page=store-inventaris" class="nav-link">Inventaris</a>
                    </li>
                    <li class="nav-item">
                      <a href="<?php echo PATH; ?>?page=store-agenda" class="nav-link">Agenda Store</a>
                    </li>
                    <!-- <li class="nav-item">
                      <a href="<?php echo PATH; ?>?page=store-voucher" class="nav-link">Voucher & Gift</a>
                    </li> -->
                  <?php endif; ?>
                </ul>
              </div>
            </li>
            <?php if ($selectedstore!==""): ?>
            <li class="nav-item">
              <a class="nav-link" href="#navbar-examples" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-examples">
                <i class="ni ni-basket text-orange"></i>
                <span class="nav-link-text">Bahan</span>
              </a>
              <div class="collapse <?php if ($dir=="bahan") echo "show"; ?>" id="navbar-examples">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=bahan-jenis" class="nav-link">Jenis Bahan</a>
                  </li>
                </ul>
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=bahan-gudang" class="nav-link">Bahan Gudang</a>
                  </li>
                </ul>
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=bahan-bar" class="nav-link">Bahan Bar</a>
                  </li>
                </ul>
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=bahan-operasional" class="nav-link">Bahan Operasional</a>
                  </li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#navbar-components" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-components">
                <i class="ni ni-app text-info"></i>
                <span class="nav-link-text">Produk</span>
              </a>
              <div class="collapse" id="navbar-components">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=produk-kategori" class="nav-link">Kategori Produk</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=produk-produk" class="nav-link">Data Produk</a>
                  </li>

                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#navbar-forms" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-forms">
                <i class="ni ni-circle-08 text-pink"></i>
                <span class="nav-link-text">Personalia</span>
              </a>
              <div class="collapse" id="navbar-forms">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=personalia-konsumen" class="nav-link">Data Customer</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=personalia-supplier" class="nav-link">Data Supplier</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=personalia-bon" class="nav-link">Data Bon</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=personalia-lemburan" class="nav-link">Data Lemburan</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=personalia-jadwal" class="nav-link">Jadwal Kerja</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=personalia-jadwal" class="nav-link">Payroll</a>
                  </li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#navbar-tables" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-tables">
                <i class="ni ni-box-2 text-default"></i>
                <span class="nav-link-text">Gudang</span>
              </a>
              <div class="collapse" id="navbar-tables">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=gudang-order" class="nav-link">Order Bahan Gudang</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=gudang-opname" class="nav-link">Opname Bahan Gudang</a>
                  </li>
                  <!-- <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=gudang-jadwal" class="nav-link">Laporan Gudang</a>
                  </li> -->
                </ul>
              </div>
            </li>
            <?php endif; ?>
          </ul>
          <?php if ($selectedstore!==""): ?>
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">Keuangan</h6>
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="#navbar-pengeluaran" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                <i class="ni ni-delivery-fast text-primary"></i>
                <span class="nav-link-text">Pengeluaran</span>
              </a>
              <div class="collapse" id="navbar-pengeluaran">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=belanja-harian" class="nav-link">Belanja Harian</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=belanja-bulanan" class="nav-link">Belanja Bulanan</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=belanja-operasional" class="nav-link">Belanja Operasional</a>
                  </li>

                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#navbar-pemasukan" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                <i class="ni ni-trophy text-primary"></i>
                <span class="nav-link-text">Pemasukan</span>
              </a>
              <div class="collapse" id="navbar-pemasukan">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="<?php echo PATH; ?>?page=pemasukan-pemasukan" class="nav-link">Data Pemasukan Lain</a>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
          <!-- Divider -->
          <hr class="my-3">
          <!-- Heading -->
          <h6 class="navbar-heading p-0 text-muted">Laporan</h6>
          <!-- Navigation -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="#navbar-laporantransaksi" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                <i class="ni ni-single-copy-04 text-primary"></i>
                <span class="nav-link-text">Pendapatan</span>
              </a>
              <div class="collapse" id="navbar-laporantransaksi">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="../../pages/maps/google.html" class="nav-link">Pendapatan Harian</a>
                  </li>
                  <li class="nav-item">
                    <a href="../../pages/maps/google.html" class="nav-link">Pendapatan Bulanan</a>
                  </li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#navbar-laporanall" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                <i class="ni ni-support-16 text-primary"></i>
                <span class="nav-link-text">Laporan Lain</span>
              </a>
              <div class="collapse" id="navbar-laporanall">
                <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
                    <a href="../../pages/maps/google.html" class="nav-link">Laporan Pembayaran</a>
                  </li>
                  <li class="nav-item">
                    <a href="../../pages/maps/google.html" class="nav-link">Laporan Produk</a>
                  </li>
                </ul>
              </div>
            </li>
          </ul>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </nav>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <nav class="navbar navbar-top navbar-expand navbar-dark  bg-gradient-success border-bottom">
      <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <button onclick="location.href='index.php';"  class="btn btn-icon btn-warning" type="button">
            <span class="btn-inner--icon"><i class="ni ni-bold-left"></i></span>
          </button>

          <button onClick="window.location.reload();"  class="btn btn-icon btn-warning" type="button">
            <span class="btn-inner--icon"><i class="ni ni-app"></i></span>
          </button>

          <a id="printtes" href="#" class="btn btn-icon btn-warning" type="button">
            <span class="btn-inner--icon"><i class="ni ni-support-16"></i> Cek Printer </span>
          </a>

          <ul class="navbar-nav align-items-center ml-md-auto">

          </ul>
          <ul class="navbar-nav align-items-center ml-auto ml-md-0">
            <li class="nav-item dropdown">
              <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="media align-items-center">
                  <span class="avatar avatar-sm rounded-circle">
                    <img alt="Image placeholder" src="../assets/avatar/<?php echo $_SESSION["login"]->foto; ?>">
                  </span>
                  <div class="media-body ml-2 d-none d-lg-block">
                    <span class="mb-0 text-sm  font-weight-bold"><?php echo "Hallo,".$_SESSION["login"]->nadep ?></span>
                  </div>
                </div>
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                <div class="dropdown-header noti-title">
                  <h6 class="text-overflow m-0">Selamat Bekerja!</h6>
                </div>
                <a href="<?php echo SITE_URL; ?>?page=main-user&&action=setting"  class="dropdown-item">
                  <i class="ni ni-single-02"></i>
                  <span>Profile & Setting</span>
                </a>
                <div class="dropdown-divider"></div>
                <a href="<?php echo PATH; ?>?page=main-login&&action=logout" class="dropdown-item">
                  <i class="ni ni-user-run"></i>
                  <span>Logout</span>
                </a>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <?php
        $view = new View($viewName);
        $view->bind('data', $data);
        $view->forceRender();
    ?>
  </div>

</body>

<!-- Argon Scripts -->

<!-- Optional JS -->
<script src="../resource/vendor/chart.js/dist/Chart.min.js"></script>
<script src="../resource/vendor/chart.js/dist/Chart.extension.js"></script>
<script src="../resource/vendor/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../resource/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../resource/vendor/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="../resource/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="../resource/vendor/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="../resource/vendor/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="../resource/vendor/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="../resource/vendor/datatables.net-select/js/dataTables.select.min.js"></script>
<script src="../resource/vendor/bootstrap-notify/bootstrap-notify.min.js"></script>


<!-- Argon JS -->
<script src="../resource/js/argon.js"></script>
<!-- Demo JS - remove this in your project -->
<script src="../resource/js/demo.min.js"></script>

</html>

<script type="text/javascript">
  $("#printtes").on("click", function() {
    PrintInterface.printSome("Cek","TES DARI KELUD");
  });
</script>
