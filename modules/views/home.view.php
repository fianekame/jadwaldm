<div class="header bg-gradient-success pb-6">
  <div class="container-fluid">
    <div class="header-body">
    </div>
  </div>
</div>
<!-- <a id="printtes" class="btn btn-sm btn-neutral">Coba Koneksi Printer</a> -->
<!-- Page content -->
<div class="container-fluid mt--5">
  <div class="row">
    <div class="col-lg-12">
      <div class="row">
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                    <i class="ni ni-cart"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">POS Transaksi</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>?page=pos-transaksi" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                    <i class="ni ni-basket"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">POS Casier</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>?page=pos-kasir" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                    <i class="ni ni-album-2"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">Our Menu</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>?page=produk-produk" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                    <i class="ni ni-basket"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">Belanja Harian</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>?page=belanja-harian" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                    <i class="ni ni-basket"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">Belanja Bulanan</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>?page=belanja-bulanan" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                    <i class="ni ni-basket"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">Belanja Operasional</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>?page=belanja-operasional" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                    <i class="ni ni-box-2"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">Bahan Bar</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>?page=bahan-bar" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                    <i class="ni ni-box-2"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">Bahan Gudang</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>?page=bahan-gudang" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                    <i class="ni ni-delivery-fast"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">Order Bahan</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>?page=gudang-order" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                    <i class="ni ni-box-2"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">Data Kustomer</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>?page=personalia-konsumen" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                    <i class="ni ni-calendar-grid-58"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">Jadwal Kerja</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>?page=personalia-jadwal" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card">
            <div class="card-body">
              <div class="row align-items-center">
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow">
                    <i class="ni ni-calendar-grid-58"></i>
                  </div>
                </div>
                <div class="col ml--2">
                  <h4 class="mb-0">
                    <a href="#!">Jadwal Event</a>
                  </h4>
                  <span class="text-success">●</span>
                  <small>Active</small>
                </div>
                <div class="col-auto">
                  <a href="<?php echo PATH; ?>" type="button" class="btn btn-sm btn-success">Open</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
</div>

<script type="text/javascript">
  $("#printtes").on("click", function() {
    PrintInterface.printSome("Cek","TES DARI KELUD");
  });
</script>
