<?php
/**
 * @Author  : David Naista<davidnaista83@gmail.com>
 * @Date    : 12/29/15 - 7:12 PM
 */

class LoginController extends Controller
{
    public function index()
    {
        $login = isset($_SESSION["login"]) ? $_SESSION["login"] : "";
        $type = isset($_SESSION["type"]) ? $_SESSION["type"] : "";
        if ($login && $type==1) {
            $this->redirect("index.php");
        }
        $message = array();
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $message = array(
                'success'   => false,
                'message'   => 'Maaf Username/Password Salah.'
            );
            $username = isset($_POST["username"]) ? $_POST["username"] : "";
            $password = isset($_POST["password"]) ? $_POST["password"] : "";
            $this->model('dokter');
            $user = $this->dokter->getWhere(array(
                'npm' => $username,
                'verifikasi' => '1',
                'passdm' => md5($password)
            ));

            if (count($user) > 0) {
                $message    = array(
                    'success'   => true,
                    'message'   => 'Selamat anda berhasil login.'
                );
                $this->model('dokter');
                $data1 = $this->dokter->getJoin(array('instansi'),
                    array(
                      'doktermuda.idinstansi' => 'instansi.idinstansi'
                    ),
                    'JOIN',
                    array(
                      'doktermuda.iddm' => $user[0]->iddm
                    )
                );
                $_SESSION["login"] = $data1[0];
                $_SESSION["iddm"] = $data1[0]->iddm;
                echo '<meta http-equiv="refresh" content="1;url=index.php">';
            }
        }
        $view = $this->view('login')->bind('message', $message);
    }

    public function logout()
    {
        unset($_SESSION["login"]);
        $this->redirect('index.php');
    }
}
