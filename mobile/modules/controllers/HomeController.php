<?php

use \modules\controllers\MainController;

class HomeController extends MainController {

    public function index() {
        $id = $_SESSION["iddm"];
        $idinstitusi = $_SESSION["login"]->idinstansi;
        $tgl = date('Y-m-d');
        $nexttgl = date('Y-m-d', strtotime(' +1 day'));

        $this->model('jadwal');
        $data = $this->jadwal->customSql(
          "SELECT*FROM jadwal JOIN ruangan JOIN doktermuda JOIN instansi
          ON jadwal.idruangan = ruangan.idruangan AND doktermuda.idinstansi = instansi.idinstansi
          AND jadwal.iddm = doktermuda.iddm WHERE
          '".$tgl."' >= jadwal.tglmulai and '".$tgl."' <= jadwal.tglselesai and jadwal.iddm = '".$id."' order by ruangan.namaruangan ASC"
        );

        $data2 = $this->jadwal->customSql(
          "SELECT*FROM jadwal JOIN ruangan JOIN doktermuda JOIN instansi
          ON jadwal.idruangan = ruangan.idruangan AND doktermuda.idinstansi = instansi.idinstansi
          AND jadwal.iddm = doktermuda.iddm WHERE
          '".$nexttgl."' >= jadwal.tglmulai and '".$nexttgl."' <= jadwal.tglselesai and jadwal.iddm = '".$id."' order by ruangan.namaruangan ASC"
        );

        $data1 = $this->jadwal->customSql(
          "SELECT*FROM jadwal JOIN ruangan JOIN doktermuda JOIN instansi
          ON jadwal.idruangan = ruangan.idruangan AND doktermuda.idinstansi = instansi.idinstansi
          AND jadwal.iddm = doktermuda.iddm WHERE
          '".$tgl."' >= jadwal.tglmulai and '".$tgl."' <= jadwal.tglselesai and jadwal.idruangan = '".$data[0]->idruangan."' and jadwal.iddm != '".$id."' and doktermuda.idinstansi = '".$idinstitusi."' order by ruangan.namaruangan ASC"
        );

        $this->template('home', array("jadwal"=>$data, "kelompok"=>$data1, "selanjutnya"=>$data2,  "tgl"=>$tgl));
    }
}
?>
