<?php

use \modules\controllers\MainController;

class LaporanController extends MainController {

    public function harian() {
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $this->model('jadwal');
        $id = $_SESSION["iddm"];
        $tgl = isset($_POST["tglmulai"]) ? $_POST["tglmulai"] : "";
        $idinstitusi = $_SESSION["login"]->idinstansi;

        $this->model('jadwal');
        $data = $this->jadwal->customSql(
          "SELECT*FROM jadwal JOIN ruangan JOIN doktermuda JOIN instansi
          ON jadwal.idruangan = ruangan.idruangan AND doktermuda.idinstansi = instansi.idinstansi
          AND jadwal.iddm = doktermuda.iddm WHERE
          '".$tgl."' >= jadwal.tglmulai and '".$tgl."' <= jadwal.tglselesai and jadwal.iddm = '".$id."' order by ruangan.namaruangan ASC"
        );

        $data1 = $this->jadwal->customSql(
          "SELECT*FROM jadwal JOIN ruangan JOIN doktermuda JOIN instansi
          ON jadwal.idruangan = ruangan.idruangan AND doktermuda.idinstansi = instansi.idinstansi
          AND jadwal.iddm = doktermuda.iddm WHERE
          '".$tgl."' >= jadwal.tglmulai and '".$tgl."' <= jadwal.tglselesai and jadwal.idruangan = '".$data[0]->idruangan."' and jadwal.iddm != '".$id."' and doktermuda.idinstansi = '".$idinstitusi."' order by ruangan.namaruangan ASC"
        );


        $this->template('laporan/harian', array("jadwal"=>$data, "kelompok"=>$data1, "tgl"=>$tgl));

      }else{

        $this->template('laporan/harian', array());

      }
    }

    public function all() {
        $this->model('jadwal');
        $id = $_SESSION["iddm"];

        $this->model('jadwal');
        $data = $this->jadwal->customSql(
          "SELECT*FROM jadwal JOIN ruangan JOIN doktermuda JOIN instansi
          ON jadwal.idruangan = ruangan.idruangan AND doktermuda.idinstansi = instansi.idinstansi
          AND jadwal.iddm = doktermuda.iddm WHERE jadwal.iddm = '".$id."' order by jadwal.tglmulai ASC"
        );

        $this->template('laporan/ruang', array("jadwal"=>$data));

    }


    public function nilai() {

        $this->template('laporan/nilai', array());

    }

    public function teman() {
      $id = $_SESSION["iddm"];
      $idinstitusi = $_SESSION["login"]->idinstansi;

      $this->model('dokter');
      $data = $this->dokter->getJoin(array('instansi'),
          array(
            'doktermuda.idinstansi' => 'instansi.idinstansi'
          ),
          'JOIN',
          array(
            'doktermuda.idinstansi' => $idinstitusi
          )
      );

      $this->template('laporan/teman', array("teman"=>$data));

    }


}
?>
