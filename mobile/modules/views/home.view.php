<div class="section mt-3 mb-3">
    <div class="card">
        <div class="card-body ">
          <div class="profile-head">
              <div class="avatar">
                  <img src="../resource/mobile/img/sample/avatar/avatar1.jpg" alt="avatar" class="imaged w64 rounded">
              </div>
              <div class="in">
                  <h3 class="name"><?php echo $data["login"]->namadm; ?></h3>
                  <h5 class="subtext"><?php echo $data["login"]->namainstansi; ?></h5>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="section full mt-2">
    <div class="listview-title mt-2">Jadwal Kamu Hari Ini : <?php echo tanggal_indo($data["tgl"],True); ?></div>
    <div class="wide-block pt-2 pb-2">
      <h4> Ruangan : <?php echo $data["jadwal"][0]->namaruangan; ?> </h4>
      <div class="chip chip-media">
          <i class="chip-icon bg-success">
              <ion-icon name="alarm"></ion-icon>
          </i>
          <span class="chip-label"><?php echo tanggal_indo($data["jadwal"][0]->tglmulai,True); ?></span>
      </div>
      <div class="chip chip-media">
          <i class="chip-icon bg-warning">
              <ion-icon name="alarm"></ion-icon>
          </i>
          <span class="chip-label"><?php echo tanggal_indo($data["jadwal"][0]->tglselesai,True); ?></span>
      </div>


    </div>
</div>


<div class="listview-title mt-2">Teman Kelompok Hari Ini </div>
<ul class="listview image-listview">
  <?php foreach ($data["kelompok"] as $usr): ?>
    <li>
        <div class="item">
            <img src="../resource/mobile/img/sample/avatar/avatar3.jpg" alt="image" class="image">
            <div class="in">
                <div><?php echo $usr->namadm; ?> <br> <small> <?php echo $usr->namainstansi; ?> </small> </div>
            </div>
        </div>
    </li>
  <?php endforeach; ?>
</ul>

<div class="section full mt-2">
    <div class="listview-title mt-2">Jadwal Kamu Besok : <?php echo tanggal_indo(date('Y-m-d', strtotime(' +1 day')),True); ?></div>
    <div class="wide-block pt-2 pb-2">
      <h4> Ruangan : <?php echo $data["selanjutnya"][0]->namaruangan; ?> </h4>
      <div class="chip chip-media">
          <i class="chip-icon bg-success">
              <ion-icon name="alarm"></ion-icon>
          </i>
          <span class="chip-label"><?php echo tanggal_indo($data["selanjutnya"][0]->tglmulai,True); ?></span>
      </div>
      <div class="chip chip-media">
          <i class="chip-icon bg-warning">
              <ion-icon name="alarm"></ion-icon>
          </i>
          <span class="chip-label"><?php echo tanggal_indo($data["selanjutnya"][0]->tglselesai,True); ?></span>
      </div>


    </div>
</div>
