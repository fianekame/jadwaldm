<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Jadwal Online Dokter Muda</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="../resource/mobile/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="../resource/mobile/img/icon/192x192.png">
    <link rel="stylesheet" href="../resource/mobile/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body>

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->

    <!-- App Header -->
    <div class="appHeader bg-primary scrolled">
        <div class="left">
            <a href="#" class="headerButton" data-toggle="modal" data-target="#sidebarPanel">
                <ion-icon name="menu-outline"></ion-icon>
            </a>
        </div>
        <div class="pageTitle">
            Jadwal Online
        </div>
    </div>
    <!-- * App Header -->

    <!-- App Capsule -->
    <div id="appCapsule">
			<?php
					$view = new View($viewName);
					$view->bind('data', $data);
					$view->forceRender();
			?>
      <br>

    </div>
    <!-- * App Capsule -->


    <!-- App Bottom Menu -->
    <div class="appBottomMenu">
        <a href="index.php" class="item active">
            <div class="col">
                <ion-icon name="home-outline"></ion-icon>
            </div>
        </a>
        <a href="<?php echo PATH; ?>?page=laporan-laporan&&action=harian" class="item">
            <div class="col">
                <ion-icon name="search-outline"></ion-icon>
            </div>
        </a>
        <a href="<?php echo PATH; ?>?page=laporan-laporan&&action=all" class="item">
            <div class="col">
                <ion-icon name="layers-outline"></ion-icon>
            </div>
        </a>
        <a href="javascript:;" class="item" data-toggle="modal" data-target="#sidebarPanel">
            <div class="col">
                <ion-icon name="menu-outline"></ion-icon>
            </div>
        </a>
    </div>
    <!-- * App Bottom Menu -->

    <!-- App Sidebar -->
    <div class="modal fade panelbox panelbox-left" id="sidebarPanel" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">

                    <!-- profile box -->
                    <div class="profileBox">
                        <div class="image-wrapper">
                            <img src="../resource/mobile/img/sample/avatar/avatar1.jpg" alt="image" class="imaged rounded">
                        </div>
                        <div class="in">
                            <strong><?php echo $data["login"]->namadm; ?></strong>
                            <div class="text-muted">
                                <?php echo $data["login"]->namainstansi; ?>
                            </div>
                        </div>
                        <a href="javascript:;" class="close-sidebar-button" data-dismiss="modal">
                            <ion-icon name="close"></ion-icon>
                        </a>
                    </div>
                    <!-- * profile box -->

                    <ul class="listview flush transparent no-line image-listview mt-2">
                        <li>
                            <a href="<?php echo PATH; ?>?page=laporan-laporan&&action=all" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="calendar-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Jadwalku
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo PATH; ?>?page=laporan-laporan&&action=nilai" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="briefcase-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    Nilaiku
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo PATH; ?>?page=laporan-laporan&&action=teman" class="item">
                                <div class="icon-box bg-primary">
                                    <ion-icon name="person-add-outline"></ion-icon>
                                </div>
                                <div class="in">
                                    <div>Teman</div>
                                </div>
                            </a>
                        </li>
                    </ul>

                </div>

                <!-- sidebar buttons -->
                <div class="sidebar-buttons">
                    <!-- <a href="javascript:;" class="button">
                        <ion-icon name="person-outline"></ion-icon>
                    </a>
                    <a href="javascript:;" class="button">
                        <ion-icon name="archive-outline"></ion-icon>
                    </a>
                    <a href="javascript:;" class="button">
                        <ion-icon name="settings-outline"></ion-icon>
                    </a> -->
                    <a href="<?php echo PATH; ?>?page=main-login&&action=logout" class="button">
                        <ion-icon name="log-out-outline"></ion-icon>
                    </a>
                </div>
                <!-- * sidebar buttons -->
            </div>
        </div>
    </div>
    <!-- * App Sidebar -->



    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="../resource/mobile/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="../resource/mobile/js/lib/popper.min.js"></script>
    <script src="../resource/mobile/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="../resource/mobile/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="../resource/mobile/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="../resource/mobile/js/base.js"></script>



</body>

</html>
