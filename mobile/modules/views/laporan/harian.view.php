

<div class="section mt-2">
    <div class="card">
        <div class="card-header">
          Cari Jadwal
        </div>
        <div class="card-body">
          <form id="myForm" role="form" action="<?php echo PATH; ?>?page=laporan-laporan&&action=harian" method="post">
            <div class="form-row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-control-label" for="validationDefault01">Pilih Tanggal</label>
                  <input type="date" name="tglmulai" id="tglmulai" class="form-control" placeholder="Keterangan" required>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Cek Jadwal</button>
          </form>
        </div>
    </div>
</div>
<?php if (isset($data["jadwal"])): ?>
  <?php if (!empty($data["jadwal"])): ?>
    <div class="section mt-2">
        <div class="card text-white bg-success">
            <div class="card-body">
              <b>
                <?php echo "Jadwal Untuk : ". tanggal_indo($data["tgl"],True); ?>
              </b>
            </div>
        </div>
    </div>
    <div class="section mt-2">
        <div class="card">
            <div class="card-body">
                <h4> Ruangan : <?php echo $data["jadwal"][0]->namaruangan; ?> </h4>
                <div class="chip chip-media">
                    <i class="chip-icon bg-success">
                        <ion-icon name="alarm"></ion-icon>
                    </i>
                    <span class="chip-label"><?php echo tanggal_indo($data["jadwal"][0]->tglmulai,True); ?></span>
                </div>
                <div class="chip chip-media">
                    <i class="chip-icon bg-warning">
                        <ion-icon name="alarm"></ion-icon>
                    </i>
                    <span class="chip-label"><?php echo tanggal_indo($data["jadwal"][0]->tglselesai,True); ?></span>
                </div>

              <div class="mt-2">Teman Kelompok Hari Ini </div>
              <ul class="listview image-listview">
                <?php foreach ($data["kelompok"] as $usr): ?>
                  <li>
                      <div class="item">
                          <img src="../resource/mobile/img/sample/avatar/avatar3.jpg" alt="image" class="image">
                          <div class="in">
                              <div><?php echo $usr->namadm; ?> <br> <small> <?php echo $usr->namainstansi; ?> </small> </div>
                          </div>
                      </div>
                  </li>
                <?php endforeach; ?>
              </ul>
            </div>
        </div>
    </div>
  <?php else: ?>
    <div class="section mt-2">
        <div class="card text-white bg-danger">
            <div class="card-body">
              <b>
                <?php echo "Jadwal Tidak Ditemukan Untuk : ". tanggal_indo($data["tgl"],True); ?>
              </b>
            </div>
        </div>
    </div>
  <?php endif; ?>
<?php endif; ?>
