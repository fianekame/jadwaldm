<div class="header-large-title">
    <h1 class="title">Jadwalku</h1>
</div>
<?php if (isset($data["jadwal"])): ?>
  <?php if (!empty($data["jadwal"])): ?>
    <?php foreach ($data["jadwal"] as $usr): ?>
      <div class="section mt-2">
          <div class="card">
              <div class="card-body">
                  <h4> Ruangan : <?php echo $usr->namaruangan; ?> </h4>
                  <div class="chip chip-media">
                      <i class="chip-icon bg-success">
                          <ion-icon name="alarm"></ion-icon>
                      </i>
                      <span class="chip-label"><?php echo tanggal_indo($usr->tglmulai,False); ?></span>
                  </div>
                  <div class="chip chip-media">
                      <i class="chip-icon bg-warning">
                          <ion-icon name="alarm"></ion-icon>
                      </i>
                      <span class="chip-label"><?php echo tanggal_indo($usr->tglselesai,False); ?></span>
                  </div>
              </div>
          </div>
      </div>
    <?php endforeach; ?>
  <?php else: ?>
    <div class="section mt-2">
        <div class="card text-white bg-danger">
            <div class="card-body">
              <b>
                <?php echo "Belum Ada Jadwal"; ?>
              </b>
            </div>
        </div>
    </div>
  <?php endif; ?>
<?php endif; ?>
