<div class="header-large-title">
    <h1 class="title">Temanku</h1>
</div>
<div class="listview-title mt-2"></div>
<ul class="listview image-listview">
  <?php $idnya = $_SESSION["iddm"]; ?>
    <?php foreach ($data["teman"] as $usr): ?>
      <?php if ($usr->iddm != $idnya): ?>
        <li>
            <a href="#" class="item">
                <img src="../resource/mobile/img/sample/avatar/avatar1.jpg" alt="image" class="image">
                <div class="in">
                    <div>
                        <?php echo $usr->namadm; ?>
                        <footer><?php echo $usr->namainstansi; ?></footer>
                    </div>
                </div>
            </a>
        </li>
      <?php endif; ?>

    <?php endforeach; ?>
</ul>
