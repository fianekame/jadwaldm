<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="theme-color" content="#000000">
    <title>Login Dokter Muda</title>
    <meta name="description" content="Mobilekit HTML Mobile UI Kit">
    <meta name="keywords" content="bootstrap 4, mobile template, cordova, phonegap, mobile, html" />
    <link rel="icon" type="image/png" href="../resource/mobile/img/favicon.png" sizes="32x32">
    <link rel="apple-touch-icon" sizes="180x180" href="../resource/mobile/img/icon/192x192.png">
    <link rel="stylesheet" href="../resource/mobile/css/style.css">
    <link rel="manifest" href="__manifest.json">
</head>

<body class="bg-white">

    <!-- loader -->
    <div id="loader">
        <div class="spinner-border text-primary" role="status"></div>
    </div>
    <!-- * loader -->


    <!-- App Capsule -->
    <div id="appCapsule" class="pt-4">

        <div class="login-form mt-3">
            <div class="section">
                <img src="../resource/mobile/img/sample/photo/vector4.png" alt="image" class="form-image">
            </div>
            <div class="section mt-1">
                <h1>Jadwal Online</h1>
                <h4>Cek jadwal stase kamu</h4>
            </div>
            <div class="section mt-1 mb-5">
							<?php
							if(count($message)) {
							?>
							<?php if ($message["success"] == true): ?>
								<div class="alert alert-success alert-dismissible fade show" role="alert">
									<span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
									<span class="alert-inner--text"><strong>Success!</strong> Login Berhasil</span>
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
											<span aria-hidden="true">&times;</span>
									</button>
								</div>
							<?php else: ?>
								<div class="alert alert-danger alert-dismissible fade show" role="alert">
										<span class="alert-inner--icon"><i class="ni ni-like-2"></i></span>
										<span class="alert-inner--text"><strong>Warning!</strong> Kesalahan Pada Proses Login!</span>
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">
												<span aria-hidden="true">&times;</span>
										</button>
								</div>
							<?php endif; ?>
							<?php
							}
							?>
							<form method="post" class="forms-sample">
                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="text" class="form-control" id="username" name="username" required placeholder="Email address">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>

                    <div class="form-group boxed">
                        <div class="input-wrapper">
                            <input type="password" class="form-control" id="password" name="password" required placeholder="Password">
                            <i class="clear-input">
                                <ion-icon name="close-circle"></ion-icon>
                            </i>
                        </div>
                    </div>

										<div class="form-button-group">
												<button type="submit" class="btn btn-primary btn-block btn-lg">Log in</button>
										</div>

                </form>
            </div>
        </div>


    </div>
    <!-- * App Capsule -->



    <!-- ///////////// Js Files ////////////////////  -->
    <!-- Jquery -->
    <script src="../resource/mobile/js/lib/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap-->
    <script src="../resource/mobile/js/lib/popper.min.js"></script>
    <script src="../resource/mobile/js/lib/bootstrap.min.js"></script>
    <!-- Ionicons -->
    <script type="module" src="https://unpkg.com/ionicons@5.0.0/dist/ionicons/ionicons.js"></script>
    <!-- Owl Carousel -->
    <script src="../resource/mobile/js/plugins/owl-carousel/owl.carousel.min.js"></script>
    <!-- jQuery Circle Progress -->
    <script src="../resource/mobile/js/plugins/jquery-circle-progress/circle-progress.min.js"></script>
    <!-- Base Js File -->
    <script src="../resource/mobile/js/base.js"></script>


</body>

</html>
