<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Jadwal Dokter Muda</title>
	<!-- core:css -->
	<link rel="stylesheet" href="../resource/assets/vendors/core/core.css">

	<!-- endinject -->
	<!-- plugin css for this page -->
  <link rel="stylesheet" href="../resource/assets/vendors/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../resource/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
	<link rel="stylesheet" href="../resource/assets/vendors/select2/select2.min.css">

	<!-- end plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="../resource/assets/fonts/feather-font/css/iconfont.css">
	<link rel="stylesheet" href="../resource/assets/vendors/flag-icon-css/css/flag-icon.min.css">
	<!-- endinject -->
  <!-- Layout styles -->
	<link rel="stylesheet" href="../resource/assets/css/demo_5/style.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="../resource/assets/images/favicon.png" />
  <script src="../resource/assets/vendors/jquery/jquery.min.js"></script>


</head>
<body>
	<div class="main-wrapper">

		<!-- partial:../../partials/_navbar.html -->
		<div class="horizontal-menu">
			<nav class="navbar top-navbar">
				<div class="container">
					<div class="navbar-content">
						<a class="navbar-brand" href="#">
							<img src="../resource/assets/images/syamrabu.png" width="50" height="50" alt="">
						</a>
						<a href="#" class="navbar-brand">
							Jadwal<span> Dokter Muda</span>
						</a>
						<ul class="navbar-nav">
							<li class="nav-item dropdown nav-profile">
								<a class="nav-link dropdown-toggle" href="#" id="profileDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<img src="../resource/assets/images/default.jpeg" alt="profile">
								</a>
								<div class="dropdown-menu" aria-labelledby="profileDropdown">
									<div class="dropdown-header d-flex flex-column align-items-center">
										<div class="figure mb-3">
											<img src="../resource/assets/images/default.jpeg" alt="">
										</div>
										<div class="info text-center">
											<p class="name font-weight-bold mb-0">Administrator</p>
										</div>
									</div>
									<div class="dropdown-body">
										<ul class="profile-nav p-0 pt-3">
											<li class="nav-item">
												<a href="<?php echo PATH; ?>?page=main-login&&action=logout" class="nav-link">
													<i data-feather="log-out"></i>
													<span>Keluar</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</li>
						</ul>
						<button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
							<i data-feather="menu"></i>
						</button>
					</div>
				</div>
			</nav>
			<nav class="bottom-navbar">
				<div class="container">
					<ul class="nav page-navigation">
						<li class="nav-item">
							<a class="nav-link" href="index.php">
								<i class="link-icon" data-feather="box"></i>
								<span class="menu-title">Dashboard</span>
							</a>
						</li>
            <li class="nav-item">
							<a class="nav-link" href="<?php echo PATH; ?>?page=master-ruangan">
								<i class="link-icon" data-feather="box"></i>
								<span class="menu-title">Nama KSM</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo PATH; ?>?page=master-instansi">
								<i class="link-icon" data-feather="box"></i>
								<span class="menu-title">Data Institusi</span>
							</a>
						</li>
            <li class="nav-item">
							<a class="nav-link" href="<?php echo PATH; ?>?page=master-dokter">
								<i class="link-icon" data-feather="box"></i>
								<span class="menu-title">Data Dokter Muda</span>
							</a>
						</li>
            <li class="nav-item">
							<a class="nav-link" href="<?php echo PATH; ?>?page=master-jadwal">
								<i class="link-icon" data-feather="box"></i>
								<span class="menu-title">Penjadwalan Stase</span>
							</a>
						</li>
            <li class="nav-item">
							<a class="nav-link" href="<?php echo PATH; ?>?page=laporan-laporan&&action=harian">
								<i class="link-icon" data-feather="box"></i>
								<span class="menu-title">Jadwal Harian</span>
							</a>
						</li>
            <li class="nav-item">
							<a class="nav-link" href="<?php echo PATH; ?>?page=laporan-laporan&&action=perruang">
								<i class="link-icon" data-feather="box"></i>
								<span class="menu-title">Jadwal KSM</span>
							</a>
						</li>
					</ul>
				</div>
			</nav>
		</div>
		<!-- partial -->

		<div class="page-wrapper">

			<div class="page-content">
        <?php
            $view = new View($viewName);
            $view->bind('data', $data);
            $view->forceRender();
        ?>
			</div>

			<!-- partial:../../partials/_footer.html -->
			<footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
				<p class="text-muted text-center text-md-left mb-0 d-none d-md-block">Handcrafted With <i class="mb-1 text-primary ml-1 icon-small" data-feather="heart"></i></p>
			</footer>
			<!-- partial -->

		</div>
	</div>

	<!-- core:js -->
	<script src="../resource/assets/vendors/core/core.js"></script>
	<!-- endinject -->
	<!-- plugin js for this page -->
  <script src="../resource/assets/vendors/jquery-ui/jquery-ui.min.js"></script>
	<script src="../resource/assets/vendors/select2/select2.min.js"></script>
  <script src="../resource/assets/vendors/moment/moment.min.js"></script>
  <script src="../resource/assets/vendors/fullcalendar/fullcalendar.min.js"></script>
  <script src="../resource/assets/vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="../resource/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
	<!-- end plugin js for this page -->
	<!-- inject:js -->
	<script src="../resource/assets/vendors/feather-icons/feather.min.js"></script>
	<script src="../resource/assets/js/template.js"></script>
	<!-- endinject -->
	<!-- custom js for this page -->
  <script src="../resource/assets/js/fullcalendar.js"></script>
	<script src="../resource/assets/js/select2.js"></script>

  <script src="../resource/assets/js/data-table.js"></script>
  <!-- end custom js for this page -->
</body>
</html>
