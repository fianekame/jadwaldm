<h3 class="page-title">Data Master Jadwal</h3>
<hr>
<div class="row">
  <div class="col-12 col-md-12">
    <div class="table-responsive">
      <button type="button" data-id="add" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
        Tambah Jadwal
      </button>

      <hr>
      <table class="table table-bordered" id="dataTableExample" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
            <th>Npm</th>
            <th>Nama</th>
            <th>Institusi</th>
            <th>Ruangan</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
            <th>Npm</th>
            <th>Nama</th>
            <th>Institusi</th>
            <th>Ruangan</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($data["jadwal"] as $usr): ?>
            <?php $datas = $usr->idjadwal."/".$usr->idruangan."/".$usr->iddm."/".$usr->tglmulai."/".$usr->tglselesai; ?>
            <tr>
              <td><?php echo $no; ?></td>
              <td>
                <?php echo tanggal_indo($usr->tglmulai); ?>
              </td>
              <td>
                <?php echo tanggal_indo($usr->tglselesai); ?>
              </td>
              <td>
                <?php echo $usr->npm; ?>
              </td>
              <td>
                <?php echo $usr->namadm; ?>
              </td>
              <td>
                <?php echo $usr->namainstansi; ?>
              </td>
              <td>
                <?php echo $usr->namaruangan; ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#exampleModalCenter" data-id="<?php echo $datas; ?>" class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-wrench"></i>
                    </span>
                    <span class="text">Edit</span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=master-jadwal&&action=delete&&id=<?php echo $usr->idjadwal; ?>" onclick="return confirm('Data Akan Di Hapus ?');" class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Hapus</span>
                </a>
              </td>
            </tr>
            <?php $no = $no+1; ?>
          <?php endforeach; ?>
        </tbody>
      </table>

      <!-- Modal -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Jadwal</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <form id="myForm" role="form" action="<?php echo PATH; ?>?page=master-jadwal&&action=addchange" method="post">
                <div class="form-row">
                  <div class="col-md-12">
                    <input type="hidden" name="id" id="id" class="form-control">
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label">Nama Ruangan</label>
                      <select name="idruangan" id="idruangan" class="form-control js-example-basic-single">
                        <?php foreach ($data["ruangan"] as $ba): ?>
                          <option value="<?php echo $ba->idruangan; ?>"><?php echo $ba->namaruangan;?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label">Nama Dokter</label>
                      <select name="iddm" id="iddm" class="form-control js-example-basic-single1">
                        <?php foreach ($data["dokter"] as $ba): ?>
                          <option value="<?php echo $ba->iddm; ?>"><?php echo $ba->npm."-".$ba->namadm;?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label" for="validationDefault01">Tangal Mulai</label>
                      <input type="date" name="tglmulai" id="tglmulai" class="form-control" placeholder="Keterangan" required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label" for="validationDefault01">Tanggal Selesai</label>
                      <input type="date" name="tglselesai" id="tglselesai" class="form-control" placeholder="Keterangan" required>
                    </div>
                  </div>
                </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#exampleModalCenter').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            document.getElementById("myForm").reset();
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              $("#idruangan").select2("val", data[1]);
              $("#iddm").select2("val", data[2]);
              document.getElementById('tglmulai').value= data[3];
              document.getElementById('tglselesai').value= data[4];
            }

         });
    });
  </script>
