
<h3 class="page-title">Data Master Ruangan</h3>
<hr>
<div class="row">
  <div class="col-12 col-md-12">
    <div class="table-responsive">
      <button type="button" data-id="add" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
        Tambah Ruangan
      </button>

      <hr>
      <table class="table table-bordered" id="dataTableExample" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Ruangan</th>
            <th>Keterangan</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Nama Ruangan</th>
            <th>Keterangan</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($data["ruangan"] as $usr): ?>
            <?php $datas = $usr->idruangan."/".$usr->namaruangan."/".$usr->keterangan; ?>
            <tr>
              <td>
                <?php echo $no; ?>
              </td>
              <td>
                <?php echo $usr->namaruangan; ?>
              </td>
              <td>
                <?php echo $usr->keterangan; ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#exampleModalCenter" data-id="<?php echo $datas; ?>" class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-wrench"></i>
                    </span>
                    <span class="text">Edit</span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=master-ruangan&&action=delete&&id=<?php echo $usr->idruangan; ?>" onclick="return confirm('Data Akan Di Hapus ?');" class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Hapus</span>
                </a>
              </td>
            </tr>
            <?php $no = $no +1; ?>
          <?php endforeach; ?>
        </tbody>
      </table>

      <!-- Modal -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Ruangan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="myForm" role="form" action="<?php echo PATH; ?>?page=master-ruangan&&action=addchange" method="post">
                <div class="form-row">
                  <div class="col-md-12">
                    <input type="hidden" name="id" id="id" class="form-control">
                    <div class="form-group">
                      <label class="form-control-label" for="validationDefault01">Nama Ruangan</label>
                      <input type="text" name="namaruang" id="namaruang" class="form-control" placeholder="Nama Ruangan" required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label" for="validationDefault01">Keterangan Ruangan</label>
                      <input type="text" name="ketruang" id="ketruang" class="form-control" placeholder="Keterangan" required>
                    </div>
                  </div>
                </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#exampleModalCenter').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            document.getElementById("myForm").reset();
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('namaruang').value= data[1];
              document.getElementById('ketruang').value= data[2];
            }

         });
    });
  </script>
