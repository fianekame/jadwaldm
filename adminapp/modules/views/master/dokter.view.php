
<h3 class="page-title">Data Master Dokter</h3>
<hr>
<div class="row">
  <div class="col-12 col-md-12">
    <div class="table-responsive">
      <button type="button" data-id="add" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
        Tambah Dokter
      </button>

      <hr>
      <table class="table table-bordered" id="dataTableExample" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Foto</th>
            <th>NPM</th>
            <th>Nama Dokter</th>
            <th>Institusi</th>
            <th>Tindakan</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Foto</th>
            <th>NPM</th>
            <th>Nama Dokter</th>
            <th>Institusi</th>
            <th>Tindakan</th>
            <th>Aksi</th>
          </tr>
        </tfoot>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($data["dokter"] as $usr): ?>
            <?php $datas = $usr->iddm."/".$usr->foto."/".$usr->npm."/".$usr->namadm."/".$usr->idinstansi; ?>
            <tr>
              <td><?php echo $no; ?></td>
              <td>
                <img src="../resource/assets/images/<?php echo $usr->foto; ?>" alt="profile">

              </td>
              <td>
                <?php echo $usr->npm; ?>
              </td>
              <td>
                <?php echo $usr->namadm; ?>
              </td>
              <td>
                <?php echo $usr->namainstansi; ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#exampleModalCenter" data-id="<?php echo $datas; ?>" class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-wrench"></i>
                    </span>
                    <span class="text">Edit</span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=master-dokter&&action=delete&&id=<?php echo $usr->iddm; ?>" onclick="return confirm('Data Akan Di Hapus ?');" class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Hapus</span>
                </a>
              </td>
              <td>
                <a href="<?php echo SITE_URL; ?>?page=master-dokter&&action=reset&&npm=<?php echo $usr->npm; ?>&&id=<?php echo $usr->iddm; ?>" onclick="return confirm('Password Akan Direset ?');" class="btn btn-warning btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Reset</span>
                </a>
                <?php if ( $usr->verifikasi == 0): ?>
                  <a href="<?php echo SITE_URL; ?>?page=master-dokter&&action=verif&&id=<?php echo $usr->iddm; ?>" onclick="return confirm('Verifikasi Akun Ini ?');" class="btn btn-info btn-icon-split btn-sm">
                      <span class="icon text-white-50">
                        <i class="fas fa-trash"></i>
                      </span>
                      <span class="text">Verifikasi</span>
                  </a>
                <?php else: ?>
                  <a href="<?php echo SITE_URL; ?>?page=master-dokter&&action=unverif&&id=<?php echo $usr->iddm; ?>" onclick="return confirm('Batalkan Verifikasi Akun Ini ?');" class="btn btn-success btn-icon-split btn-sm">
                      <span class="icon text-white-50">
                        <i class="fas fa-trash"></i>
                      </span>
                      <span class="text">Tangguhkan</span>
                  </a>
                <?php endif; ?>
              </td>
            </tr>
            <?php $no = $no+1; ?>
          <?php endforeach; ?>
        </tbody>
      </table>

      <!-- Modal -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Dokter</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="myForm" role="form" action="<?php echo PATH; ?>?page=master-dokter&&action=addchange" method="post">
                <div class="form-row">
                  <div class="col-md-12">
                    <input type="hidden" name="id" id="id" class="form-control">
                    <div class="form-group">
                      <label class="form-control-label" for="validationDefault01">NPM Dokter</label>
                      <input type="text" name="npm" id="npm" class="form-control" placeholder="Nama Ruangan" required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label" for="validationDefault01">Nama Dokter</label>
                      <input type="text" name="namadm" id="namadm" class="form-control" placeholder="Keterangan" required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label">Asal Instansi</label>
                      <select name="idinstansi" id="idinstansi" class="form-control js-example-basic-single">
                        <?php foreach ($data["instansi"] as $ba): ?>
                          <option value="<?php echo $ba->idinstansi; ?>"><?php echo $ba->namainstansi;?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <hr>
                  <span>*Password Akan Secara Default Adalah NPM Dokter.</span>
                  <br><br>
                </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#exampleModalCenter').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            document.getElementById("myForm").reset();
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('npm').value= data[2];
              document.getElementById('namadm').value= data[3];
              $("#idinstansi").select2("val", data[4]);

            }

         });
    });
  </script>
