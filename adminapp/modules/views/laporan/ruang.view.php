
<h3 class="page-title">Jadwal Per Stase : <?php echo tanggal_indo(date('Y-m-d'), True); ?></h3>
<hr>

<div class="row">
  <?php foreach ($data["jadwal"] as $jd): ?>
    <div class="col-md-4 pb-4">
      <div class="card">
      <div class="card-header">
        <h4><?php echo $jd["namaruang"]; ?></h4>
      </div>
      <?php if (empty($jd["jadwal"])): ?>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">Tidak Ada Jadwal</li>
        </ul>
      <?php else: ?>
        <table class="table" width="100%" cellspacing="0">
          <tbody>
            <?php foreach ($jd["jadwal"] as $usr): ?>
              <tr>
                <td align="right" width="10%"> <img src="../resource/assets/images/<?php echo $usr->foto; ?>" alt="profile"></td>
                <td width="60%">
                  <b>
                    <?php echo  $usr->namadm; ?>
                  </b>
                  <br>
                  <?php echo $usr->namainstansi; ?>
                </td>
              </tr>
              <?php $no = $no+1; ?>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php endif; ?>
    </div>
    </div>
  <?php endforeach; ?>

</div>
