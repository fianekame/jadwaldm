
<h3 class="page-title">Laporan Jadwal Berdasarkan Hari</h3>
<hr>
<div class="row">
  <div class="col-12 col-md-12">
    <form id="myForm" role="form" action="<?php echo PATH; ?>?page=laporan-laporan&&action=harian" method="post">
      <div class="form-row">
        <div class="col-md-12">
          <div class="form-group">
            <label class="form-control-label" for="validationDefault01">Pilih Tanggal</label>
            <input type="date" name="tglmulai" id="tglmulai" class="form-control" placeholder="Keterangan" required>
          </div>
        </div>
      </div>
      <button type="submit" class="btn btn-primary">Cek Jadwal</button>
    </form>
  </div>
</div>
<hr>
<div class="row">
  <?php if (!empty($data["jadwal"])): ?>
    <div class="col-12 col-md-12">
      <h4>Jadwal Dokter Tanggal : <b><?php echo tanggal_indo($data["tgl"]); ?></b> </h4>
      <br>
      <table class="table table-striped" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
            <th>Npm</th>
            <th>Nama</th>
            <th>Institusi</th>
            <th>Ruangan</th>
          </tr>
        </thead>
        <!-- <tfoot>
          <tr>
            <th>No</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
            <th>Npm</th>
            <th>Nama</th>
            <th>Ruangan</th>
          </tr>
        </tfoot> -->
        <tbody>
          <?php foreach ($data["jadwal"] as $usr): ?>
            <tr>
              <td> <img src="../resource/assets/images/<?php echo $usr->foto; ?>" alt="profile"></td>
              <td>
                <?php echo tanggal_indo($usr->tglmulai); ?>
              </td>
              <td>
                <?php echo tanggal_indo($usr->tglselesai); ?>
              </td>
              <td>
                <?php echo $usr->npm; ?>
              </td>
              <td>
                <?php echo $usr->namadm; ?>
              </td>
              <td>
                <?php echo $usr->namainstansi; ?>
              </td>
              <td>
                <?php echo $usr->namaruangan; ?>
              </td>
            </tr>
            <?php $no = $no+1; ?>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  <?php endif; ?>

</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#exampleModalCenter').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            document.getElementById("myForm").reset();
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('npm').value= data[2];
              document.getElementById('namadm').value= data[3];
            }

         });
    });
  </script>
