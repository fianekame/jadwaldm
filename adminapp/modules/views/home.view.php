
<h1 class="page-title">Home</h1>
<p class="lead">Jadwal Dokter Hari Ini</p>

<hr>

<div class="row">
  <?php if (!empty($data["jadwal"])): ?>
    <div class="col-12 col-md-12">
      <h4>Jadwal Dokter Tanggal : <b><?php echo tanggal_indo($data["tgl"]); ?></b> </h4>
      <br>
      <table class="table table-striped" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
            <th>Npm</th>
            <th>Nama</th>
            <th>Institusi</th>
            <th>Ruangan</th>
          </tr>
        </thead>
        <!-- <tfoot>
          <tr>
            <th>No</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
            <th>Npm</th>
            <th>Nama</th>
            <th>Ruangan</th>
          </tr>
        </tfoot> -->
        <tbody>
          <?php foreach ($data["jadwal"] as $usr): ?>
            <tr>
              <td> <img src="../resource/assets/images/<?php echo $usr->foto; ?>" alt="profile"></td>
              <td>
                <?php echo tanggal_indo($usr->tglmulai); ?>
              </td>
              <td>
                <?php echo tanggal_indo($usr->tglselesai); ?>
              </td>
              <td>
                <?php echo $usr->npm; ?>
              </td>
              <td>
                <?php echo $usr->namadm; ?>
              </td>
              <td>
                <?php echo $usr->namainstansi; ?>
              </td>
              <td>
                <?php echo $usr->namaruangan; ?>
              </td>
            </tr>
            <?php $no = $no+1; ?>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  <?php endif; ?>
</div>
