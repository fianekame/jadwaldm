<?php

use \modules\controllers\MainController;

class LaporanController extends MainController {

    public function harian() {
      if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $this->model('jadwal');
        $tgl = isset($_POST["tglmulai"]) ? $_POST["tglmulai"] : "";
        $data = $this->jadwal->customSql(
          "SELECT*FROM jadwal JOIN ruangan JOIN doktermuda JOIN instansi
          ON jadwal.idruangan = ruangan.idruangan AND doktermuda.idinstansi = instansi.idinstansi
          AND jadwal.iddm = doktermuda.iddm WHERE
          '".$tgl."' >= jadwal.tglmulai and '".$tgl."' <= jadwal.tglselesai order by ruangan.namaruangan ASC"
        );
        $this->template('laporan/harian', array("jadwal"=>$data, "tgl"=>$tgl));

      }else{

        $this->template('laporan/harian', array());

      }
    }

    public function perruang() {
      $tgl = date('Y-m-d');
      // $tgl = "2021-03-05";
      $this->model('ruangan');
      $data1 = $this->ruangan->get();
      $datanya = array();
      foreach ($data1 as $r){
        $data = $this->ruangan->customSql(
          "SELECT*FROM jadwal JOIN ruangan JOIN doktermuda JOIN instansi
          ON jadwal.idruangan = ruangan.idruangan AND doktermuda.idinstansi = instansi.idinstansi
          AND jadwal.iddm = doktermuda.iddm WHERE
          '".$tgl."' >= jadwal.tglmulai and '".$tgl."' <= jadwal.tglselesai and ruangan.idruangan = '".$r->idruangan."'"
        );
        array_push($datanya,array("namaruang"=>$r->namaruangan,"jadwal"=>$data));

      }
      $this->template('laporan/ruang', array("jadwal"=>$datanya));

    }

}
?>
