<?php

use \modules\controllers\MainController;

class RuanganController extends MainController {

    public function index() {
      $this->model('ruangan');
      $data1 = $this->ruangan->getOrder("namaruangan ASC");
      $this->template('master/ruangan', array("ruangan"=>$data1));
    }

    public function addchange() {
        $this->model('ruangan');
        $error      = array();
        $success    = null;
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $id = isset($_POST["id"]) ? $_POST["id"] : "";
            $namaruang  = isset($_POST["namaruang"]) ? $_POST["namaruang"] : "";
            $ketruang = isset($_POST["ketruang"]) ? $_POST["ketruang"] : "";

            if (empty($id)) {
              if(count($error) == 0) {
                  $insert = $this->ruangan->insert(
                      array(
                        'namaruangan' => $namaruang,
                        'keterangan' => $ketruang
                      )
                  );
                  if($insert) {
                      $success = "Data Berhasil di ditambahkan.";
                  }
              }
            } else {
              $updateArrayData = array(
                'namaruangan' => $namaruang,
                'keterangan' => $ketruang
              );
              if(count($error) == 0) {
                  $update = $this->ruangan->update($updateArrayData, array('idruangan' => $id));
                  if($update) {
                      $success = "Data berhasil di rubah.";
                  }
              }
            }
        }
        $this->back();
        // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
    }

    public function delete()
    {
        $id = isset($_GET["id"]) ? $_GET["id"] : 0;
        $this->model('ruangan');
        $delete = $this->ruangan->delete(array('idruangan' => $id));
        if ($delete) {
            $this->back();
        }
    }
}
?>
