<?php

use \modules\controllers\MainController;

class DokterController extends MainController {

    public function index() {
      $this->model('dokter');
      $data1 = $this->dokter->getJoin(array('instansi'),
          array(
            'doktermuda.idinstansi' => 'instansi.idinstansi'
          ),
          'JOIN'
      );
      $this->model('instansi');
      $data2 = $this->instansi->get();
      $this->template('master/dokter', array("dokter"=>$data1, "instansi"=>$data2));
    }

    public function addchange() {
        $this->model('dokter');
        $error      = array();
        $success    = null;
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $id = isset($_POST["id"]) ? $_POST["id"] : "";
            $npm  = isset($_POST["npm"]) ? $_POST["npm"] : "";
            $namadm = isset($_POST["namadm"]) ? $_POST["namadm"] : "";
            $idinstansi = isset($_POST["idinstansi"]) ? $_POST["idinstansi"] : "";



            if (empty($id)) {
              if(count($error) == 0) {
                  $insert = $this->dokter->insert(
                      array(
                        'npm' => $npm,
                        'passdm' => md5($npm),
                        'idinstansi' => $idinstansi,
                        'namadm' => $namadm
                      )
                  );
                  if($insert) {
                      $success = "Data Berhasil di ditambahkan.";
                  }
              }
            } else {
              $updateArrayData = array(
                'npm' => $npm,
                'idinstansi' => $idinstansi,
                'namadm' => $namadm
              );
              if(count($error) == 0) {
                  $update = $this->dokter->update($updateArrayData, array('iddm' => $id));
                  if($update) {
                      $success = "Data berhasil di rubah.";
                  }
              }
            }
        }
        $this->back();
        // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
    }

    public function delete()
    {
        $id = isset($_GET["id"]) ? $_GET["id"] : 0;
        $this->model('dokter');
        $delete = $this->dokter->delete(array('iddm' => $id));
        if ($delete) {
            $this->back();
        }
    }

    public function reset()
    {
      $password = isset($_GET["npm"]) ? $_GET["npm"] : 0;
      $id = isset($_GET["id"]) ? $_GET["id"] : 0;
      $this->model('dokter');
      $update = $this->dokter->update(array('passdm' => md5($password)), array('iddm' => $id));
      if($update) {
        $this->back();
      }
    }

    public function verif()
    {
      $id = isset($_GET["id"]) ? $_GET["id"] : 0;
      $this->model('dokter');
      $update = $this->dokter->update(array('verifikasi' => '1'), array('iddm' => $id));
      if($update) {
        $this->back();
      }
    }

    public function unverif()
    {
      $id = isset($_GET["id"]) ? $_GET["id"] : 0;
      $this->model('dokter');
      $update = $this->dokter->update(array('verifikasi' => '0'), array('iddm' => $id));
      if($update) {
        $this->back();
      }
    }
}
?>
