<?php

use \modules\controllers\MainController;

class JadwalController extends MainController {

    public function index() {
      $this->model('jadwal');
      // $data1 = $this->jadwal->get();
      $data1 = $this->jadwal->getJoin(array('ruangan','doktermuda', 'instansi'),
          array(
            'jadwal.idruangan' => 'ruangan.idruangan',
            'doktermuda.idinstansi' => 'instansi.idinstansi',
            'jadwal.iddm' => 'doktermuda.iddm'
          ),
          'JOIN'
      );
      $this->model('ruangan');
      $data2 = $this->ruangan->get();

      $this->model('dokter');
      $data3 = $this->dokter->get();
      $this->template('master/jadwal', array("jadwal"=>$data1,"ruangan"=>$data2,"dokter"=>$data3));
    }

    public function addchange() {
        $this->model('jadwal');
        $error      = array();
        $success    = null;
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $id = isset($_POST["id"]) ? $_POST["id"] : "";
            $idruangan  = isset($_POST["idruangan"]) ? $_POST["idruangan"] : "";
            $iddm = isset($_POST["iddm"]) ? $_POST["iddm"] : "";
            $tglmulai = isset($_POST["tglmulai"]) ? $_POST["tglmulai"] : "";
            $tglselesai = isset($_POST["tglselesai"]) ? $_POST["tglselesai"] : "";



            if (empty($id)) {
              if(count($error) == 0) {
                  $insert = $this->jadwal->insert(
                      array(
                        'idruangan' => $idruangan,
                        'iddm' => $iddm,
                        'tglmulai' => $tglmulai,
                        'tglselesai' => $tglselesai
                      )
                  );
                  if($insert) {
                      $success = "Data Berhasil di ditambahkan.";
                  }
              }
            } else {
              $updateArrayData = array(
                'idruangan' => $idruangan,
                'iddm' => $iddm,
                'tglmulai' => $tglmulai,
                'tglselesai' => $tglselesai
              );
              if(count($error) == 0) {
                  $update = $this->jadwal->update($updateArrayData, array('idjadwal' => $id));
                  if($update) {
                      $success = "Data berhasil di rubah.";
                  }
              }
            }
        }
        $this->back();
        // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
    }

    public function delete()
    {
        $id = isset($_GET["id"]) ? $_GET["id"] : 0;
        $this->model('jadwal');
        $delete = $this->jadwal->delete(array('idjadwal' => $id));
        if ($delete) {
            $this->back();
        }
    }

}
?>
