<?php

use \modules\controllers\MainController;

class InstansiController extends MainController {

    public function index() {
      $this->model('instansi');
      $data1 = $this->instansi->get();
      $this->template('master/instansi', array("instansi"=>$data1));
    }

    public function addchange() {
        $this->model('instansi');
        $error      = array();
        $success    = null;
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $id = isset($_POST["id"]) ? $_POST["id"] : "";
            $namaruang  = isset($_POST["nama"]) ? $_POST["nama"] : "";

            if (empty($id)) {
              if(count($error) == 0) {
                  $insert = $this->instansi->insert(
                      array(
                        'namainstansi' => $namaruang
                      )
                  );
                  if($insert) {
                      $success = "Data Berhasil di ditambahkan.";
                  }
              }
            } else {
              $updateArrayData = array(
                'namainstansi' => $namaruang
              );
              if(count($error) == 0) {
                  $update = $this->instansi->update($updateArrayData, array('idinstansi' => $id));
                  if($update) {
                      $success = "Data berhasil di rubah.";
                  }
              }
            }
        }
        $this->back();
        // $this->template('frmkategori', array('error' => $error, 'success' => $success,'title' => 'Tambah Kategori Produk'));
    }

    public function delete()
    {
        $id = isset($_GET["id"]) ? $_GET["id"] : 0;
        $this->model('instansi');
        $delete = $this->instansi->delete(array('idinstansi' => $id));
        if ($delete) {
            $this->back();
        }
    }
}
?>
