<?php

use \modules\controllers\MainController;

class HomeController extends MainController {

    public function index() {
        $tgl = date('Y-m-d');
        $this->model('jadwal');
        $data = $this->jadwal->customSql(
          "SELECT*FROM jadwal JOIN ruangan JOIN doktermuda JOIN instansi
          ON jadwal.idruangan = ruangan.idruangan AND doktermuda.idinstansi = instansi.idinstansi
          AND jadwal.iddm = doktermuda.iddm WHERE
          '".$tgl."' >= jadwal.tglmulai and '".$tgl."' <= jadwal.tglselesai order by ruangan.namaruangan ASC"
        );
        $this->template('home', array("jadwal"=>$data, "tgl"=>$tgl));
    }
}
?>
