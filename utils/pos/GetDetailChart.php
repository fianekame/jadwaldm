<?php
session_start();
include("../../config.php");
include("../../library/mylib.php");
$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME) or die("connection failed".mysqli_errno());
$idt = $_GET['idt'];
$status = $_GET['status'];

$sql ="SELECT * FROM transaksi, user WHERE transaksi.iduser = user.iduser and transaksi.idtrans =".$idt;
$resulttrans =mysqli_query($con,$sql);
$rowtrans = mysqli_fetch_assoc($resulttrans);

$date = date('d-m-Y',strtotime($rowtrans['tglorder']));
$time = date('H:i:s',strtotime($rowtrans['tglorder']));
$sql = "";

if ($status=="0") {
  $sql ="SELECT * FROM transtemp, produk WHERE transtemp.idproduk = produk.idproduk and transtemp.idtrans =".$idt;
  $result =mysqli_query($con,$sql);
} else {
  $sql ="SELECT * FROM transdetail, produk WHERE transdetail.idproduk = produk.idproduk and transdetail.idtrans =".$idt;
  $result =mysqli_query($con,$sql);
}

$tojson =mysqli_query($con,$sql);
while($row = mysqli_fetch_assoc($tojson)) {
	$data[] = $row;
}
$jsondata = json_encode(array("trans"=>$rowtrans, "item"=>$data));
?>
  <table class="table table-bordered">
    <tr>
      <th colspan="5">
        <?php echo $date ?> / <?php echo $time ?> <br>
        <?php echo "INV-KLD-".$rowtrans['idtrans'] ?> | <?php echo "Nomor: ".$rowtrans['nomeja'] ?> | <?php echo $rowtrans['atasnama'] ?>
      </th>
    </tr>
    <tr>
      <td colspan="5">Total Belanja : <b> <?php echo rupiah((int)$rowtrans['grandtotal']); ?> </b> </td>
    </tr>
    <tr>
      <td>No</td>
      <td>Nama</td>
      <td>Qty</td>
      <td>Harga</td>
      <td>Total</td>
    </tr>
    <?php
    $jml = 0;
    $no = 1;
    // output data of each row
    while($row = mysqli_fetch_assoc($result)) {
    ?>
      <tr style="border-bottom: 1px solid grey">
        <td><?php echo $no; ?></td>
        <td><?php echo $row["namaproduk"]."(".$row["varianbeli"].")"; ?></td>
        <td><?php echo $row["jmlbeli"]; ?></td>
        <td><?php echo rupiah($row["harga"]); ?></td>
        <td><?php echo rupiah((int)$row["harga"] * $row["jmlbeli"]); ?></td>
      </tr>
    <?php
    $no++;
    }
    ?>
  </table>
  <button id="printbill" type="button" class="btn btn-danger"> Cetak Bill </button>
  <button id="printorder" type="button" class="btn btn-warning"> Cetak Order </button>

  <script type="text/javascript">
    $("#printbill").on("click", function(){
      console.log(JSON.stringify(<?php echo $jsondata; ?>));
      PrintInterface.printSome("PrintBill",JSON.stringify(<?php echo $jsondata; ?>));
    });
    $("#printorder").on("click", function(){
      console.log(JSON.stringify(<?php echo $jsondata; ?>));
      PrintInterface.printSome("PrintOrder",JSON.stringify(<?php echo $jsondata; ?>));
    });
  </script>
