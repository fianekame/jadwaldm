<div class="profile-page tx-13">
  <div class="row">
    <div class="col-12 grid-margin">
      <div class="profile-header">
        <div class="cover">
          <div class="gray-shade"></div>
          <figure>
            <img src="images/bg_logo.png" class="img-fluid" alt="profile cover">
          </figure>
        </div>
        <div class="header-links">
        </div>
      </div>
    </div>
  </div>
  <div class="row profile-body">
    <!-- middle wrapper start -->
    <div class="col-md-12 col-xl-12 middle-wrapper">
      <div class="row">
        <div class="col-md-12 grid-margin">
          <div class="card rounded">
            <div class="card-header">
              <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                  <div class="ml-2">
                    <h5>Pendaftaran Data Dokter Muda</h5>
                    <p class="tx-11 text-muted">RSUD Syamrabu Bangkalan</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body">
              <form id="myForm" role="form" action="<?php echo PATH; ?>?page=main-home&&action=daftar" method="post">
                <div class="form-row">
                  <div class="col-md-12">
                    <input type="hidden" name="id" id="id" class="form-control">
                    <div class="form-group">
                      <label class="form-control-label" for="validationDefault01">NPM Dokter</label>
                      <input type="text" name="npm" id="npm" class="form-control" placeholder="Nama Ruangan" required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label" for="validationDefault01">Nama Dokter</label>
                      <input type="text" name="namadm" id="namadm" class="form-control" placeholder="Keterangan" required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="form-control-label">Asal Instansi</label>
                      <select name="idinstansi" id="idinstansi" class="form-control js-example-basic-single">
                        <?php foreach ($data["instansi"] as $ba): ?>
                          <option value="<?php echo $ba->idinstansi."-".$ba->namainstansi; ?>"><?php echo $ba->namainstansi;?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <hr>
                    <button type="submit" class="btn btn-block btn-primary">Daftar</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- <div class="card-footer">
              <div class="d-flex post-actions">
                <a href="javascript:;" class="d-flex align-items-center text-muted mr-4">
                  <i class="icon-md" data-feather="info"></i>
                  <p class="d-none d-md-block ml-2">Lihat Cara Pendaftaran</p>
                </a>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
