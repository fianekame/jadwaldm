<div class="profile-page tx-13">
  <div class="row">
    <div class="col-12 grid-margin">
      <div class="profile-header">
        <div class="cover">
          <div class="gray-shade"></div>
          <figure>
            <img src="images/bg_logo.png" class="img-fluid" alt="profile cover">
          </figure>
        </div>
        <div class="header-links">
        </div>
      </div>
    </div>
  </div>
  <div class="row profile-body">
    <!-- middle wrapper start -->
    <div class="col-md-12 col-xl-12 middle-wrapper">
      <div class="row">
        <div class="col-md-12 grid-margin">
          <div class="card rounded">
            <div class="card-header">
              <div class="d-flex align-items-center justify-content-between">
                <div class="d-flex align-items-center">
                  <div class="ml-2">
                    <h5>Pendaftaran Data Dokter Muda</h5>
                    <p class="tx-11 text-muted">RSUD Syamrabu Bangkalan</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body">
              <?php if (isset($data["error"]) && count($data["error"]) > 0) {?>
                <div class="card text-white bg-danger">
                  <div class="card-header"> <h5>Gagal Mendaftar</h5> </div>
                  <div class="card-body text-white">
                    <h5 class="">Ada Kesalahan Pendaftaran Pada Data : </h5>
                    <br>
                    <ul>
                      <li>NPM  : <?php echo $data["datanya"][0]; ?></li>
                      <li>Nama : <?php echo $data["datanya"][1]; ?></li>
                      <li>Intitusi : <?php echo $data["datanya"][2]; ?></li>
                    </ul>
                    <p class="card-text"><?php echo $data["error"][0] ?> <br><br>Silahkan Coba Dengan Data Lain, Atau Tanyakan Pada Petugas.</p>
                  </div>
                </div>
              <?php } elseif (isset($data["success"])) {?>
                <div class="card text-white bg-success">
                  <div class="card-header"> <h5>Pendaftaran Berhasil</h5> </div>
                  <div class="card-body text-white">
                    <h5 class="">Data Yang Berhasil Ditambahkan : </h5>
                    <br>
                    <ul>
                      <li>NPM  : <?php echo $data["datanya"][0]; ?></li>
                      <li>Nama : <?php echo $data["datanya"][1]; ?></li>
                      <li>Intitusi : <?php echo $data["datanya"][2]; ?></li>
                    </ul>
                    <p class="card-text"><?php echo $data["success"];?></p>

                  </div>
                </div>
              <?php } ?>

            </div>
            <div class="card-footer">
              <div class="d-flex post-actions">
                <a href="index.php" class="d-flex align-items-center text-muted mr-4">
                  <i class="icon-md" data-feather="arrow-left"></i>
                  <p class="d-none d-md-block ml-2">Kembali Ke Home</p>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
