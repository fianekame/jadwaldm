<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Pendaftaran Dokter Muda</title>
	<!-- core:css -->
	<link rel="stylesheet" href="../resource/assets/vendors/core/core.css">

	<!-- endinject -->
	<!-- plugin css for this page -->
  <link rel="stylesheet" href="../resource/assets/vendors/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../resource/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css">
	<link rel="stylesheet" href="../resource/assets/vendors/select2/select2.min.css">

	<!-- end plugin css for this page -->
	<!-- inject:css -->
	<link rel="stylesheet" href="../resource/assets/fonts/feather-font/css/iconfont.css">
	<link rel="stylesheet" href="../resource/assets/vendors/flag-icon-css/css/flag-icon.min.css">
	<!-- endinject -->
  <!-- Layout styles -->
	<link rel="stylesheet" href="../resource/assets/css/demo_5/style.css">
  <!-- End layout styles -->
  <link rel="shortcut icon" href="../resource/assets/images/favicon.png" />
  <script src="../resource/assets/vendors/jquery/jquery.min.js"></script>


</head>
<body>
	<div class="main-wrapper">

		<div class="page-wrapper">

			<div class="page-content">
        <?php
            $view = new View($viewName);
            $view->bind('data', $data);
            $view->forceRender();
        ?>
			</div>


		</div>
	</div>

	<!-- core:js -->
	<script src="../resource/assets/vendors/core/core.js"></script>
	<!-- endinject -->
	<!-- plugin js for this page -->
  <script src="../resource/assets/vendors/jquery-ui/jquery-ui.min.js"></script>
	<script src="../resource/assets/vendors/select2/select2.min.js"></script>
  <script src="../resource/assets/vendors/moment/moment.min.js"></script>
  <script src="../resource/assets/vendors/fullcalendar/fullcalendar.min.js"></script>
  <script src="../resource/assets/vendors/datatables.net/jquery.dataTables.js"></script>
  <script src="../resource/assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
	<!-- end plugin js for this page -->
	<!-- inject:js -->
	<script src="../resource/assets/vendors/feather-icons/feather.min.js"></script>
	<script src="../resource/assets/js/template.js"></script>
	<!-- endinject -->
	<!-- custom js for this page -->
  <script src="../resource/assets/js/fullcalendar.js"></script>
	<script src="../resource/assets/js/select2.js"></script>

  <script src="../resource/assets/js/data-table.js"></script>
  <!-- end custom js for this page -->
</body>
</html>
