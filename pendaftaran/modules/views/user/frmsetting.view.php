<!-- Header -->
<div class="header bg-gradient-success pb-6">
  <div class="container-fluid">
    <div class="header-body">
      <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
          <h6 class="h2 text-white d-inline-block mb-0">Setting</h6>
          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
              <li class="breadcrumb-item"><a href="index.php"><i class="fas fa-home"></i></a></li>
              <li class="breadcrumb-item active" aria-current="page">Profile Dan Ubah Password</li>
            </ol>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Page content -->
<div class="container-fluid mt--6">
  <!-- Table -->
  <div class="row">

    <div class="col-xl-6">
      <div class="card card-profile">
        <img src="../resource/img/theme/profile-cover.jpg" alt="Image placeholder" class="card-img-top">
        <div class="row justify-content-center">
          <div class="col-lg-3 order-lg-2">
            <div class="card-profile-image">
              <a href="#">
                <img src="../assets/avatar/<?php echo $data["user"]->foto; ?>" class="rounded-circle">
              </a>
            </div>
          </div>
        </div>
        <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
        </div>
        <div class="card-body pt-5">
          <div class="text-center">
            <h5 class="h3">
              <?php echo $data["login"]->nadep." ".$data["user"]->nabel; ?><span class="font-weight-light"></span>
            </h5>
            <div class="h5 font-weight-300">
              <i class="ni location_pin mr-2"></i><?php echo $data["user"]->email ?>
            </div>
            <div class="h5 mt-4">
              <i class="ni business_briefcase-24 mr-2"></i><?php echo $data["user"]->jabatan ?>
            </div>
            <div>
              <i class="ni education_hat mr-2"></i><?php echo $data["user"]->phone ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xl-6">
      <div class="card">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col-8">
              <h3 class="mb-0">Edit profile </h3>
            </div>
          </div>
        </div>
        <div class="card-body">
            <h6 class="heading-small text-muted mb-4">Password Acount</h6>

            <div class="pl-lg-4">
              <form class="" action="<?php echo PATH; ?>?page=main-user&&action=setting" method="post">
                <div class="row">

                  <div class="col-lg-12">
                    <?php
                      if(isset($data["error"]) && count($data["error"]) > 0) {
                          ?>
                          <div class="alert alert-danger" role="alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <ul class="list-square">
                                  <?php
                                  foreach($data["error"] as $error) {
                                      ?>
                                      <li>
                                          <?php echo $error; ?>
                                      </li>
                                  <?php } ?>
                              </ul>
                          </div>
                      <?php
                      }else if(isset($data["success"])) {
                          ?>
                          <div class="alert alert-success">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <?php echo $data["success"]; ?>
                          </div>
                          <meta http-equiv="refresh" content="1;url=<?php echo PATH; ?>?page=main-login&&action=logout">
                    <?php } ?>
                    <div class="form-group">
                      <label class="form-control-label" for="input-username">Password Lama</label>
                      <input type="password" name="psdlama"  class="form-control" placeholder="" value="">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-email">Pasword Baru</label>
                      <input type="password" name="psdbaru1"  class="form-control" placeholder="">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label class="form-control-label" for="input-first-name">Ulangi Password Baru</label>
                      <input type="password" name="psdbaru2" class="form-control" placeholder="" value="">
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="form-group">
                      <button class="btn btn-primary btn-block" type="submit">Simpan</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <hr class="my-4" />
            <!-- Address -->
            <h6 class="heading-small text-muted mb-4">Password Casier</h6>
            <div class="pl-lg-4">
              <form class="" action="<?php echo PATH; ?>?page=main-user&&action=passwordkasier" method="post">
                <div class="row">
                  <div class="col-md-12">
                    <?php
                      if(isset($data["errorkasir"]) && count($data["errorkasir"]) > 0) {
                          ?>
                          <div class="alert alert-danger" role="alert">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <ul class="list-square">
                                  <?php
                                  foreach($data["errorkasir"] as $error) {
                                      ?>
                                      <li>
                                          <?php echo $error; ?>
                                      </li>
                                  <?php } ?>
                              </ul>
                          </div>
                      <?php
                    }else if(isset($data["successkasir"])) {
                          ?>
                          <div class="alert alert-success">
                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                              <?php echo $data["successkasir"]; ?>
                          </div>
                          <meta http-equiv="refresh" content="1;url=<?php echo PATH; ?>?page=main-login&&action=logout">
                    <?php } ?>
                    <div class="form-group">
                      <label class="form-control-label" for="input-address">Password Kasir</label>
                      <input name="passkasir" class="form-control" placeholder="" value="<?php echo $data["user"]->passkasir; ?>" type="text" maxlength="6">
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="form-group">
                      <button class="btn btn-primary btn-block" type="submit">Simpan</button>
                    </div>
                  </div>
                </div>
              </form>

            </div>
        </div>
      </div>
    </div>






  </div>

</div>
