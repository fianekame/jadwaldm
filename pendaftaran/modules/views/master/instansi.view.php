
<h3 class="page-title">Data Master Instansi</h3>
<hr>
<div class="row">
  <div class="col-12 col-md-12">
    <div class="table-responsive">
      <button type="button" data-id="add" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
        Tambah Instansi
      </button>

      <hr>
      <table class="table table-bordered" id="dataTableExample" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama Instansi</th>
            <th>Tindakan</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Nama Instansi</th>
            <th>Tindakan</th>
          </tr>
        </tfoot>
        <tbody>
          <?php $no = 1; ?>
          <?php foreach ($data["instansi"] as $usr): ?>
            <?php $datas = $usr->idinstansi."/".$usr->namainstansi; ?>
            <tr>
              <td>
                <?php echo $no; ?>
              </td>
              <td>
                <?php echo $usr->namainstansi; ?>
              </td>
              <td>
                <a href="" data-toggle="modal" data-target="#exampleModalCenter" data-id="<?php echo $datas; ?>" class="btn btn-primary btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-wrench"></i>
                    </span>
                    <span class="text">Edit</span>
                </a>
                <a href="<?php echo SITE_URL; ?>?page=master-instansi&&action=delete&&id=<?php echo $usr->idinstansi; ?>" onclick="return confirm('Data Akan Di Hapus ?');" class="btn btn-danger btn-icon-split btn-sm">
                    <span class="icon text-white-50">
                      <i class="fas fa-trash"></i>
                    </span>
                    <span class="text">Hapus</span>
                </a>
              </td>
            </tr>
            <?php $no = $no +1; ?>
          <?php endforeach; ?>
        </tbody>
      </table>

      <!-- Modal -->
      <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Instansi</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form id="myForm" role="form" action="<?php echo PATH; ?>?page=master-instansi&&action=addchange" method="post">
                <div class="form-row">
                  <div class="col-md-12">
                    <input type="hidden" name="id" id="id" class="form-control">
                    <div class="form-group">
                      <label class="form-control-label" for="validationDefault01">Nama Instansi</label>
                      <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Ruangan" required>
                    </div>
                  </div>
                </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#exampleModalCenter').on('show.bs.modal', function (e) {
            var rowdata = $(e.relatedTarget).data('id');
            document.getElementById("myForm").reset();
            if (rowdata=="add") {
              document.getElementById("myForm").reset();
              document.getElementById('id').value= "";
            }
            else{
              var data = rowdata.split("/");
              document.getElementById('id').value= data[0];
              document.getElementById('nama').value= data[1];
            }

         });
    });
  </script>
