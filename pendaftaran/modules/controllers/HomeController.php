<?php

use \modules\controllers\MainController;

class HomeController extends MainController {

    public function index() {
      $this->model('instansi');
      $data2 = $this->instansi->get();
      $this->template('home', array( "instansi"=>$data2));
    }


    public function daftar() {
        $this->model('dokter');
        $error      = array();
        $success    = null;
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            $npm  = isset($_POST["npm"]) ? $_POST["npm"] : "";
            $namadm = isset($_POST["namadm"]) ? $_POST["namadm"] : "";
            $idinstansi = isset($_POST["idinstansi"]) ? $_POST["idinstansi"] : "";
            $inst = explode("-",$idinstansi);
            $data2 = $this->dokter->customSql("SELECT EXISTS (SELECT * FROM doktermuda WHERE npm='".$npm."' and namadm='".$namadm."' and idinstansi='".$inst[0]."') as res");
            // array_push($error, "Pasword Baru Tidak Cocok.");
            $datamasuk = array($npm,$namadm,$inst[1]);
            if ($data2[0]->res) {
              array_push($error, "Data Telah Terdaftar, Sedang Menunggu Verifikasi");
            }
            if(count($error) == 0) {
                $insert = $this->dokter->insert(
                    array(
                      'npm' => $npm,
                      'passdm' => md5($npm),
                      'idinstansi' => $inst[0],
                      'namadm' => $namadm
                    )
                );
                if($insert) {
                    $success = "Data Berhasil Terdaftarkan, Sedang Menunggu Konfirmasi";
                }
            }

        }
        $this->template('info', array('error' => $error, 'datanya' => $datamasuk, 'success' => $success,'title' => 'Pendaftaran Dokter Muda Baru'));
    }
}
?>
