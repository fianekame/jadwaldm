<?php
session_start();
include("../../config.php");
include("../../library/mylib.php");
$con = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME) or die("connection failed".mysqli_errno());
$idt = $_GET['id'];

$sql ="SELECT * FROM `transtemp`, produk WHERE transtemp.idproduk = produk.idproduk and transtemp.idtrans =".$idt;
// echo $sql;
$result=mysqli_query($con,$sql);
?>

<?php
  $totalmenu = 0;
  $jml = 0;
?>
<?php if (mysqli_num_rows($result) > 0): ?>
  <table class="table" width=100%>
    <thead>
      <tr style="border-bottom: 1px solid grey">
        <th >Menu</th>
        <th >Harga</th>
        <th >Qty</th>
        <th >Total</th>
        <!-- <th > </th> -->
      </tr>
    </thead>
    <tbody>
      <?php
      // output data of each row
      while($row = mysqli_fetch_assoc($result)) {
        $harga = 0;
      ?>
      <tr style="border-bottom: 0px solid grey">
        <td> <a class="deldata" href="#" id="<?php echo $row["idordertemp"]."-".$row["idtrans"]; ?>"><i class="fa fa-trash"></i></a> | <?php echo $row["namaproduk"]."(".$row["varianbeli"].")"; ?></td>
        <td>
          <?php if ($row["varianbeli"]=="Hot"): ?>
            <?php echo rupiah($row["hargajualhot"]); ?>
            <?php $harga = $row["hargajualhot"] * $row["jmlbeli"]; ?>
          <?php else: ?>
            <?php echo rupiah($row["hargajualice"]); ?>
            <?php $harga = $row["hargajualice"] * $row["jmlbeli"]; ?>
          <?php endif; ?>
        </td>
        <!-- <td> <a id="data-tes" type="button" name="min"><i class="fa fa-minus"></i></a>  <a class="m-2" href="#"> <?php echo $row["qty"]; ?> </a>  <a type="button" id="changedata" name="plus"><i class="fa fa-plus"></i></a></td> -->
        <td><a class="m-2" href="#"> <?php echo $row["jmlbeli"]; ?> </a></td>
        <td><?php echo rupiah($harga); ?></td>
        <td></td>
      </tr>
      <?php
      $jml = $jml + ($harga);
      $totalmenu = $totalmenu + $row["jmlbeli"];
      }
      ?>
    </tbody>
  </table>
  <input type="hidden" id="jml" name="jml" value="<?php echo $jml; ?>">
<?php else: ?>
  <?php $jml = 0; ?>
<?php endif; ?>

<script type="text/javascript">
  var bla = $('#jml').val();
  $('#sbtotal').val(bla);
  $('.totaltext').html("Rp. "+String(<?php echo $jml; ?>));
  $('.totalmenu').html(String(<?php echo $totalmenu; ?>)+ " Menu");
  $('#jumlahbarang').val(<?php echo $jml; ?>);
  if (<?php echo $totalmenu; ?> !== 0) {
    $('#proses').fadeIn();
  } else {
    $('#proses').fadeOut();
  }
  $('.deldata').click(function(){
    var data = this.id.split("-");
    var datanya = {idtemp : data[0]};
    $.ajax({
    	type: 'GET',
      url: "utils/pos/DelCart.php",
    	data: datanya,
    	success: function(response) {
    		$('.cartview').load("utils/pos/GetCart.php?id="+data[1]);
        $('#sbtotal').val(bla);
    	}
    });
  });

</script>
